package cn.iocoder.yudao.framework.common.constant;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description：
 * @Author： fengcheng
 * @Date： 2025/1/10 16:16
 */
public class Constant {
    public static final Integer INTEGER_0 = 0;
    public static final Integer INTEGER_1 = 1;

    public static final Integer INTEGER_3 = 3;
    public static final Integer INTEGER_5 = 5;
    public static final Integer INTEGER_200 = 200;
    public static final Integer INTEGER_65535 = 65535;

    public static final String SLASHES = "/";

    /**
     * xml文件中需要替换的字符
     */
    public static final String XMLNS = "xmlns";

    /**
     * xml字符串中替换xmlns的字符
     */
    public static final String NAME_SPACE = "namespace";
    /**
     * SSL 匿名用户名称
     */
    public static final String SSL_ANONYMOUS_USERNAME = "anonymous";

    /**
     * Excel导入导出功能使用的常量
     */
    public static final String XLSX = ".xlsx";
    public static final String CSV = ".csv";
    public static final String XLS = ".xls";
    public static final String ROW_MERGE = "row_merge";
    public static final String COLUMN_MERGE = "column_merge";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final int CELL_OTHER = 0;
    public static final int CELL_ROW_MERGE = 1;
    public static final int CELL_COLUMN_MERGE = 2;
    public static final int IMG_HEIGHT = 30;
    public static final int IMG_WIDTH = 30;
    public static final int BYTES_DEFAULT_LENGTH = 10240;
    public static final String ADD_TYPE = "add";
    public static final String UPDATE_TYPE = "update";
    public static final String USERNAME_REGULAR = "^[\\w\\u4e00-\\u9fa5-]{1,256}+$";
    public static final String EMAIL_REGULAR = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
    public static final String PHONE_REGULAR = "^[1][3-9]\\d{9}$";
    public static final String NUMBER_REGULAR = "^[-\\+]?[\\d]*$";

    /**
     * SSL配置 - 用户管理 - 用户信息导入，使用常量
     */
    public static final String USER_NAME = "用户名称";
    public static final String MOBILE_PHONE_NUMBER = "用户手机号码";
    public static final String USER_EMAIL = "用户邮箱";
    public static final String EXPIRATION_DATE = "过期日期";
    public static final String GROUP_PATH = "所属用户组";
    public static final List<Object> SHEET_HEAD = Arrays.asList(USER_NAME, MOBILE_PHONE_NUMBER, USER_EMAIL, EXPIRATION_DATE, GROUP_PATH, "失败原因");
    public static final List<String> USER_IMPORT_FILEDS = Arrays.asList("username", "mobile", "email", "expireday", "groupName");
    public static final List<Object> USER_EXPORT_SHEET_HEAD = Arrays.asList(USER_NAME, MOBILE_PHONE_NUMBER, GROUP_PATH, "创建时间", EXPIRATION_DATE, "用户状态");
    public static final String USER_DEFAULT_PASSWORD = "Abcd123456";
    public static final String IMPORT_FAILED = "failed";
    public static final String IMPORT_SUCCESS = "success";
    public static final String FAILURE_REASON = "失败原因";
    public static final String FAILURE_REASON_REPEAT = "用户名重复";
    public static final String MOBILE_REPEAT = "电话号码重复";
    public static final String MOBILE_OR_USER_REPEAT = "用户名或者电话号码重复";
    public static final String FAIL_USER = "failUser";
    public static final String SSL_USER_INFO = "sslUserInfo:";
    public static final String USER_IMPORT_FILE_NAME = "用户导入失败文件";
    public static final String USER_EXPORT_FILE_NAME = "用户信息导出文件";
    public static final String USER_IMPORT_TEMPLATE_FILE_NAME = "用户信息导入模板";;
    public static final long FILE_SIZE = 5 * 1024 * 1024;

    // 分页
    public static final Integer DEFAULT_PAGE_NO = 1;
    public static final Integer DEFAULT_PAGE_SIZE = 10;
    public static final Integer DEFAULT_MAX_PAGE = 10000;
    public static final Integer NO_PID = -1;

    // 净荷版本 策略行为选择标识
    public static final String POLICY_BEHAVIOR_PAYLOAD = "Payload";
    public static final String SIGN_CERT_PATH = "/zdxlz/etc/cert/x509sign/signCert.crt";
    public static final String ENC_CERT_PATH = "/zdxlz/etc/cert/x509enc/encCert.crt";
    public static final String ROOT_CERT_PATH = "/zdxlz/etc/cert/x509ca/rootcaCert.crt";
    public static final String PQC_SIGN_CERT_PATH = "/zdxlz/etc/cert/pqc/x509sign/signCert.crt";
    public static final String PQC_ENC_CERT_PATH = "/zdxlz/etc/cert/pqc/x509enc/encCert.crt";
    public static final String PQC_ROOT_CERT_PATH = "/zdxlz/etc/cert/pqc/x509ca/rootcaCert.crt";
    public static final String RSA_SIGN_CERT_PATH = "/zdxlz/etc/cert/rsa/x509sign/signCert.crt";
    public static final String RSA_ENC_CERT_PATH = "/zdxlz/etc/cert/rsa/x509enc/encCert.crt";
    public static final String RSA_ROOT_CERT_PATH = "/zdxlz/etc/cert/rsa/x509ca/rootcaCert.crt";
    public static final String CTL_SIGN_CERT_PATH = "/zdxlz/etc/cert/x509sign/ctlSignCert.crt";
    public static final String SM2_CA_CERT = "/zdxlz/etc/cert/ca/ca.cer";
    public static final String PQC_CA_CERT = "/zdxlz/etc/cert/pqc/ca/ca.cer";
    public static final String RSA_CA_CERT = "/zdxlz/etc/cert/rsa/ca/ca.cer";
    public static final String DEV_INFO_PATH = "/etc/devinfo";
    public static final String DEV_INFO_KEY_MODEL = "Model";
    public static final String HA_CONFIG_PATH = "zdxlz.dev.ha-config";
    public static final String UPGRADE_FILE_PATH = "zdxlz.dev.upgrade-file-path";
    public static final String SIGN_CERT_TYPE = "3";
    public static final String ENC_CERT_TYPE = "6";
    public static final String ROOT_CERT_TYPE = "7";
    public static final String LO_IP_ADDRESS = "127.0.0.1";
    public static final String DEFAULT_SYS_USER_NAME = "system";
    public static final String DEFAULT_SEC_USER_NAME = "security";
    public static final String DEFAULT_AUD_USER_NAME = "auditor";
    public static final String DEFAULT_SYS_ROLE = "系统管理员";
    public static final String DEFAULT_SEC_ROLE = "安全管理员";
    public static final String DEFAULT_AUD_ROLE = "审计管理员";
    public static final List<String> DEFAULT_ROLE_LIST = Lists.newArrayList(DEFAULT_SYS_ROLE, DEFAULT_SEC_ROLE, DEFAULT_AUD_ROLE);
    public static final String AUTH_MODE_COMMAND = "0"; //登录认证类型 口令认证
    public static final String AUTH_MODE_CERT = "1"; //登录认证类型 证书认证
    public static final String DEFAULT_PASSWORD = "764d66bb690ee71e2f7e076cbe25f79f0559c2c8b4c07e3658b02b6f4ad40e38";
    public static final String RESET_DEFAULT_USER_PASSWORD = "3b2cff677c5d12f403cca31021b34044db93b960afe7087573dbd0f5774e4ff0";
    public static final String IP_OPER_ACTION_CREATE = "create";
    public static final String IP_OPER_ACTION_REPLACE = "replace";
    public static final String XC_OPERATION = "xc:operation";
    public static final String OPERATION_TYPE_UPDATE = "update";
    public static final String OPERATION_TYPE_DELETE = "delete";

    public static final String STRING_ZERO = "0";
    public static final String STRING_ONE = "1";
    public static final String STRING_32 = "32";
    public static final String STRING_TWO = "2";
    public static final String STRING_THREE = "3";

    public static final String STRING_FOUR = "4";
    public static final String STRING_FIVE = "5";
    public static final String STRING_SIX = "6";
    public static final String PROTOCOL_TCP = "TCP";
    public static final String PROTOCOL_UDP = "UDP";
    public static final String PROTOCOL_RIP = "RIP";
    public static final int PROTOCOL_RIP_PORT = 520;
    public static final String STRING_223 = "223";
    public static final String STRING_127 = "127";
    public static final String STRING_255 = "255";
    public static final int UDP_PROTOCOL_NUMBER = 17;
    public static final int TCP_PROTOCOL_NUMBER = 6;
    public static final String CERTIFICATE_BEGIN = "-----BEGIN CERTIFICATE-----";
    public static final String PKCS7_BEGIN = "-----BEGIN PKCS7-----";
    public static final String CERTIFICATE_REQUEST_BEGIN = "-----BEGIN CERTIFICATE REQUEST-----";

    public static final String CTL_CENTER_REQ_HEADER_KEY = "sign";
    public static final String ROUTE_LIST_NOT_SHOW_ETH = "ip6tnl0";
    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";
    public static final String ALTERNATIVE_NAME_PREFIX = "DNS:"; //证书请求，备用域名处理时增加前缀
    public static final String SSL_CA_CONFIG_NAME = "ca";
    public static final String SSL_WEB_PORXY_CONFIG_NAME = "web_proxy";
    public static final String SSL_WEB_PORXY_PORT_CONFIG_NAME = "web_proxy_port";
    public static final String SSL_TUNNTL_PROXY_CONFIG_NAME = "tunnel_proxy";
    public static final String SSL_TUNNTL_PROXY_PORT_CONFIG_NAME = "tunnel_proxy_port";

    public static final int CSP_SESSION_KEY_LEN = 16;
    public static final String CSP_SESSION_KEY_PATH = "/zdxlz/etc/key/session/"; //会话密钥路径
    public static final String CSP_FILL_KEY_PATH = "/zdxlz/etc/key/fill/"; //充注密钥路径
    public static final String QMS_FILL_KEY_FILE_NAME = "softkey.zip"; //QMS充注密钥名称
    public static final String CSP_SESSION_FILE_NAME = "session"; //会话密钥路径
    public static final String CSP_SESSION_KEY_FROM_PATH = "/from"; //会话密钥路径 from
    public static final String CSP_SESSION_KEY_DEFAULT_INFO_NAME = "default"; //存储会话密钥默认使用文件信息
    public static final String CSP_SESSION_KEY_TO_PATH = "/to"; //会话密钥路径 to
    public static final String CSP_SESSION_KEY_NEXT_INFO_NAME = "next"; //存储会话密钥下一次使用信息
    public static final String CSP_SESSION_KEY_FULL_GET = "ALL";
    public static final String CRL_PATH= "/zdxlz/etc/cert/crl";
    public static final String PROT_OCCUPY_ERROR_REMARK= "该端口已被使用，请换一个重试";



    public static final String CSP_URI_GET_SESSION_KEY = "/csp-service/sym/session/getKeyBatch";  //CSP获取会话密钥uri
    //    http://192.168.2.219:5310/csp-service/qsg/fillKey csp 调试地址
    public static final String CSP_URI_GET_FILL_KEY = "/csp-service/qsg/fillKey"; //CSP获取充注密钥uri
    public static final String CSP_URI_GET_TOKEN = "/csp-service/qsg/accountAuth"; //CSP获取认证token
    public static final String CSP_URI_REGISTER = "/csp-service/qsg/registerGateway"; //CSP注册
    public static final String CSP_SESSION_KEY_BUSINESS_TYPE = "QSG"; //会话密钥调用常量
    public static final String CSP_SESSION_KEY_ROLE = "caller"; //会话密钥调用常量
    public static final String SA_STATUS_LIST_CONNS_PATH = "/zdxlz/etc/ike/list-conns"; //策略状态信息
    public static final String IMPORT_CONF_CTL_INIT = "/zdxlz/etc/devman/.init_ctl"; //导入配置初始化管控标识
    public static final String IMPORT_CONF_CSP_INIT = "/zdxlz/etc/devman/.init_csp"; //导入配置初始化量子标识
    public static final String GLOBAL_CONF_FILE_PATH = "/zdxlz/etc/global.conf"; //ike全局配置文件，为了获取双机状态
    public static final String CTWING_DEFAULT_CONF_FILE_PATH = "/zdxlz/etc/devman/tywl/ctwing-default-conf.xml"; //天翼物联默认配置
    public static final String CTWING_DEFAULT_CONF_FILE_SM4_KEY = "67ab242b3230ceef5703765cfc91bcc6"; //天翼物联默认配置加密key
    //    public static final String CTWING_DEFAULT_CONF_PRODUCT_ID = "<device-id>99014438</device-id>"; //天翼物联默认配置公共产品ID
    public static final String CTWING_DEFAULT_CONF_PRODUCT_ID = "99014438"; //天翼物联默认配置公共产品ID
    public static final int GLOBAL_CONF_HA_FOLLOW = 2; //全局配置文件，双机从设备标识
    public static final int GLOBAL_CONF_HA_MASTER = 1; //全局配置文件，双机主控设备标识
    public static final int GLOBAL_CONF_HA_CLOSE = 0; //全局配置文件，双机未开启
    public static final String CSP_CSR_NAME = "zdxlz"; //注册证书时证书请求名称
    public static final String CSP_CSR_EMAIL = "zdxlz"; //注册证书时证书请求邮箱
    public static final String UNSPECIFIED_IP = "0.0.0.0";
    public static final String UNSPECIFIED_IP_MASK = "0.0.0.0/0";
    public static final String UNSPECIFIED_IPv6 = "::";
    public static final String UNSPECIFIED_IPv6_UNCOMPRESS = "0:0:0:0:0:0:0:0";
    public static final String UNSPECIFIED_IPv6_MASK = "::/0";
    public static final String WEBSOCKET_CTL_HEART_CODE = "10001"; //websocket管控中心心跳码
    public static final String WEBSOCKET_LOGIN_CODE = "10002"; //websocket退出登录返回码
    public static final String WEBSOCKET_FAIL_CODE = "00000";
    public static final String DOT_CHAR = "."; //vlan 连接字符
    public static final String WELL_NUMBER_CHAR = "#";
    public static final String COLONS_CHAR = ":"; // :连接字符
    public static final String ROUTE_IPV4 = "ipv4";
    public static final String ROUTE_IPV6 = "ipv6";
    public static final String RIB_DEFAULT_NAME_SUFFIX = "-main";
    public static final String IP_TYPE_V4 = "IPv4";
    public static final String IP_TYPE_V6 = "IPv6";
    public static final String FILE_SUFFIX_ZIP = ".zip";
    public static final String LOGIN_MODULE_NAME = "登录模块";
    public static final String HA_SYNC_MODULE_NAME = "双机同步模块";
    public static final int SSL_USER_GROUP_LEVEL = 16;
    public static final String RES_SUCCESS_MSG = "操作成功！";
    public static final String RES_ERROR_MSG = "操作失败！";
    public static final long SSL_TUNNEL_IP_POOL_SIZE = 60000;
    public static final String WIFI_DHCP_DEV = "br-lan";
    public static final String PROD_TEST_PASS = "ZDXLZ_test";
    public static final String INTERFACE_LO = "lo";
    public static final Short TUNNEL_PUBLIC_KEY_ALGORITHM_SM2 = 1;
    public static final Short TUNNEL_PUBLIC_KEY_ALGORITHM_SM2_PQC = 2;
    public static final String SM2_CA_TYPE = "sm2";
    public static final String SM9_TYPE = "sm9";
    public static final String PQC_CA_TYPE = "pqc";
    public static final String RSA_CA_TYPE = "rsa";
    public static final String AIGIS2_CA_TYPE = "aigis2";
    public static final String PQC_PQM_CA_TYPE = "pqm";
    public static final String RELOGIN_MSG = "请先登录！";
    public static final String SM2_DILITHIUM2_PQMAGIC_ALG_ID = "1.2.156.10197.999.7";
    public static final String AIGIS2_ALG_ID = "1.2.156.10197.999.12";
    public static final int ENABLE_CAPTCHA_NUMBER = 1;
    public static final String LOW_END_DEVICE_MODEL_LIST = "zdxlz.csp.low-end-device-model-list";
    public static final String QUANTUM_KEY_LOW_DEFAULT_COUNT = "zdxlz.csp.quantum-key-low-default-count";
    public static final String QUANTUM_KEY_HIGHT_DEFAULT_COUNT = "zdxlz.csp.quantum-key-hight-default-count";
    public static final String ENGINE_ALGORITHM_PATH = "/zdxlz/etc/engine/engine_algorithm.ini";
    public static final String IGNORE_INTERFACE = "eth0";

    public static final String SYNC_CONFIG_SUCCESS_INFO = "同步配置信息成功";
    public static final String SYNC_CONFIG_FAIL_INFO = "同步配置信息失败";

    //HA 配置文件类型
    public static String NETCONF_HA_CONFIG_TYPE_ROOT_CERT = "7";
    public static String NETCONF_HA_CONFIG_TYPE_ENC_CERT = "6";
    public static String NETCONF_HA_CONFIG_TYPE_SIGN_CERT = "3";
    public static String NETCONF_HA_CONFIG_TYPE_SIGN_KEY = "9";
    public static String NETCONF_HA_CONFIG_TYPE_ENC_KEY = "8";
    public static String NETCONF_HA_CONFIG_TYPE_FILL_KEY = "10";
    public static String NETCONF_HA_CONFIG_TYPE_SPLIT_CHAR = "_";
    public static String HA_CONFIG_TRANSFER_ENC_KEY = "dc9cd8093b4f3c8374c2f6e1678fe209"; // 充注密钥明文双机传输时通过key加密
    public static String DASH_CONNECTOR = "-";
    public static String DOT_CONNECTOR = ",";
    public static String GET_USE_PORT_COMMAND = "netstat -anptu | awk 'NR > 2 {print $4}' | awk -F: '{print $NF}' | sort | uniq";



    public static final Map<String, String> CERT_MAPPING = new HashMap<String, String>();
    public static final Map<String, String> CERT_SIGN_MAPPING = new HashMap<String, String>();

    public static final Map<String, String> TUNNEL_STATUS = new HashMap<String, String>();

    public static final Map<String, String> POLICY_STATUS = new HashMap<String, String>();

    public static final Map<Integer, String> IP_RES_CODE = new HashMap<Integer, String>();

    static {
        CERT_MAPPING.put(SIGN_CERT_TYPE, SIGN_CERT_PATH);
        CERT_MAPPING.put(ENC_CERT_TYPE, ENC_CERT_PATH);
        CERT_MAPPING.put(ROOT_CERT_TYPE, ROOT_CERT_PATH);

        CERT_SIGN_MAPPING.put("sign","sm2-sm3id");
        CERT_SIGN_MAPPING.put("rsa-single","rsa-single");
        CERT_SIGN_MAPPING.put("rsa-double","rsa-double");
        CERT_SIGN_MAPPING.put("rsa4096-single","rsa4096-single");
        CERT_SIGN_MAPPING.put("rsa4096-double","rsa4096-double");
        CERT_SIGN_MAPPING.put("rsa1024-single","rsa1024-single");
        CERT_SIGN_MAPPING.put("rsa1024-double","rsa1024-double");
        CERT_SIGN_MAPPING.put("pqc","sm2_dilithium2");
        CERT_SIGN_MAPPING.put("pqm","sm2_dilithium2_PQMagic");
        CERT_SIGN_MAPPING.put("aigis2","sm2_aigis2");


        TUNNEL_STATUS.put("ESTABLISHED", "建立成功");
        TUNNEL_STATUS.put("REKEYING", "正在换钥");
        TUNNEL_STATUS.put("REKEYED", "换钥成功");

        POLICY_STATUS.put("INSTALLED", "建立成功");
        POLICY_STATUS.put("REKEYING", "正在换钥");
        POLICY_STATUS.put("REKEYED", "换钥成功");


        IP_RES_CODE.put(0, "修改失败！");
        IP_RES_CODE.put(1, "修改成功!");
        IP_RES_CODE.put(2, "存在同网段包含的IP地址，请检查！");
        IP_RES_CODE.put(3, "IP地址不合法！");
        IP_RES_CODE.put(4, "IP地址不合法,以127开头的IP地址无效！");
        IP_RES_CODE.put(5, "IP地址不合法,以大于223开头的IP地址无效！");
        IP_RES_CODE.put(6, "IP地址不合法！");
        IP_RES_CODE.put(7, "IP地址已存在！");
    };
}
